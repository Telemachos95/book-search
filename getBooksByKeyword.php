<?php

$keyword = $_GET['keyword'];

$host = 'localhost';
$user = 'root';
$password = '';
$db_name = 'assignment';

header('content-type: text/xml');

$db = mysqli_connect($host, $user, $password, $db_name) or die("Couldn't connect to MySql Server!");
mysqli_select_db($db, $db_name) or die(mysqli_error());
$xml = '<?xml version="1.0"?>';
$sql = 'SELECT * FROM books WHERE author like "%' . $keyword . '%" or' . ' title like "%' . $keyword . '%" or' . ' genre like "%' . $keyword . '%" or' . ' description like "%' . $keyword . '%"';
$qr = mysqli_query($db, $sql) or die(mysqli_error());
$xml.='<catalog>';
while ($res = mysqli_fetch_array($qr)) {
    $xml.='<book id="' . $res['id'] . '"><author>' . $res['author'] . '</author><title>' . $res['title'] . '</title><genre>' . $res['genre'] . '</genre><price>' . $res['price'] . '</price><publish_date>' . $res['publish_date'] . '</publish_date><description>' . $res['description'] . '</description></book>';
}
$xml.='</catalog>';

$xml1 = simplexml_load_string($xml);
foreach( $xml1->{'book'} as $bookentry){
    echo "ID: " . $bookentry['id'] . "<br>";
    echo "AUTHOR: " . $bookentry->author . "<br>";
    echo "TITLE: " . $bookentry->title . "<br>";
    echo "GENRE: " . $bookentry->genre . "<br>";
    echo "PRICE: " . $bookentry->price . "<br>";
    echo "PUBLISH_DATE: " . $bookentry->publish_date . "<br>";
    echo "DESCRIPTION: " . $bookentry->description . "<br>";
}
?>
