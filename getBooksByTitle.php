<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Book Search</title>
        <meta charset='UTF-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    </head>
    <body>
        <?php
        $bookid = intval($_GET['bookid']);

        $host = 'localhost';
        $user = 'root';
        $password = '';
        $db_name = 'assignment';

        header('content-type: text/xml');

        $db = mysqli_connect($host, $user, $password, $db_name) or die("Couldn't connect to MySql Server!");
        mysqli_select_db($db, $db_name) or die(mysqli_error());
        $xml = '<?xml version="1.0"?>';
        $sql = 'SELECT * FROM books WHERE id =' . $bookid;
        $qr = mysqli_query($db, $sql) or die(mysqli_error());
        $xml.='<catalog>';
        while ($res = mysqli_fetch_array($qr)) {
            $xml.='<book id="' . $res['id'] . '"><author>' . $res['author'] . '</author><title>' . $res['title'] . '</title><genre>' . $res['genre'] . '</genre><price>' . $res['price'] . '</price><publish_date>' . $res['publish_date'] . '</publish_date><description>' . $res['description'] . '</description></book>';
        }
        $xml.='</catalog>';

        $xml1 = simplexml_load_string($xml);
        echo "ID: " . $xml1->book[0]['id'] . "<br>";
        echo "AUTHOR: " . $xml1->book[0]->author . "<br>";
        echo "TITLE: " .  $xml1->book[0]->title . "<br>";
        echo "GENRE: " . $xml1->book[0]->genre . "<br>";
        echo "PRICE: " . $xml1->book[0]->price . "<br>";
        echo "PUBLISH_DATE: " . $xml1->book[0]->publish_date . "<br>";
        echo "DESCRIPTION: " . $xml1->book[0]->description . "<br>"; 
        ?>
    </body>
</html>
